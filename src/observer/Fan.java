package observer;

public class Fan implements Observer{

  protected String name;

  public Fan(String name) {
    this.name = name;
    }

    @Override
  public void update(Subject subject) {
    System.out.println("I am " + name
        + " and just received the following update from "
        + subject.getName()+": "
        + subject.getLatestState());
  }



}
