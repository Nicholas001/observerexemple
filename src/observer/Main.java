package observer;

public class Main {

    public static void main(String[] args) {

        Subject subject1 = new Subject("Celine Dion");
        Subject subject2 = new Subject("Jennifer Lopez");

        Observer jon = new Fan("Jon");
        subject1.attach(jon);
        subject2.attach(jon);

        Observer jack = new Fan("Jack");
        subject1.attach(jack);
        subject2.attach(jack);

        Observer jill = new Hater("Jill");
        subject2.attach(jill);


        subject1.publishUpdate("New concert!");
        System.out.println();
        subject2.publishUpdate("New song on radio");
    }
}
