package observer;

public class Hater implements Observer {

  protected String name;

  public Hater(String name) {
    this.name = name;
  }

  @Override
  public void update(Subject subject) {
    System.out.println("I am " + name
        + " and hate this person: "
        + subject.getName() + ". "
        + subject.getLatestState() + " - it`s a bullshit news :)");
  }
}
