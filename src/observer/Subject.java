package observer;

import java.util.HashSet;
import java.util.Set;

public class Subject {

  protected Set<Observer> observers = new HashSet<>();
  protected String latestState;
  protected String name;

  public void attach(Observer observer) {
    observers.add(observer);
  }

  public void detach(Fan fan) {
    observers.remove(fan);
  }

  public void publishUpdate(String newState) {
    updateState(newState);
    notifyAllObservers(this);
  }

  private void notifyAllObservers(Subject subject) {
    for (Observer each : this.observers) {
      each.update(subject);
    }
  }

  public Subject(String name) {
    this.name = name;
  }

  public String getLatestState() {
    return latestState;
  }

  public void updateState(String newState) {
    this.latestState = newState;
  }

  public String getName() {
    return name;
  }
}
